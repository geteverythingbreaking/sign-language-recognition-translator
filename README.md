<h1>Hong Kong Sign Language Recognition Translator</h1>

An AI translator of Hong Kong Sign language with newly released “hand tracking detection” function from MediaPipe and RNN model, this translator has accuracy of 90%.

This project is for academic purpose. 
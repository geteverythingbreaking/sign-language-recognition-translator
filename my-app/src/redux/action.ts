export function DisplayWord(currentWord: string) {
    return {
      type: 'SHOW_WORD' as 'SHOW_WORD',
      currentWord
    }
  }

export type DisplayWord = ReturnType<typeof DisplayWord> 
  
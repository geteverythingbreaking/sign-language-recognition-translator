import { DisplayWord } from "./action";


export interface WordState{
  currentWord: string
}

const initialState: WordState = {
    currentWord:""
}


export const reducer = (oldState: WordState = initialState, action: DisplayWord): WordState => {
  switch (action.type) {
    case 'SHOW_WORD':
        return {
        ...oldState,
        currentWord: action.currentWord
          };
    default:
        return oldState
    }
}

import React from 'react';
import './App.css';

import { ReactMediaRecorder } from "react-media-recorder";
import VideoPreview from './preview1';
import {FileUploader} from './file_uploader_functional';
import {useSelector, useDispatch} from 'react-redux';
import {IRootState} from './store';
import {DisplayWord} from './redux/action';
// import { Modal } from './modal';
 

function App() {
  const dispatch = useDispatch();
  let  currentWord = useSelector((state:IRootState)=>state.word);
  // const [modalOn, setModalOn] = useState(false);

  return (
    <>
      {/* <Modal show={modalOn}/> */}
      <div>
          
        <div className="App">
          <header className="App-header">
            <div className="logo">
              Sign Language Recognition Translator
            </div>
            <div className="logo">
              AI手語翻譯器
            </div>
          </header>
        </div>

        <div className="Section-0">
          <div className="Translate-word"> Translate  from  Sign  Language </div>
          <div className="Translate-word"> Translate  into  Cantonese </div>
        </div>

        

        <div className="Section-1">
          <div className="Section-2">
            <div className="Section-3">
              <div>
                <ReactMediaRecorder
                  blobPropertyBag={{
                    type: "video/mp4"
                  }}
                  onStop={async (blobUrl: string, blob: Blob) => {
                    let file = new File([blob], 'test.mp4',
                    );
                    // setModalOn(true);
                    const formData = new FormData();
                    formData.append('profile', file);
                    const res = await fetch('http://localhost:8080/newPost', {
                      method: "POST",
                      body: formData,
                    });
                    // console.log(res)
                    const resultObject = await res.json()
                    console.log(resultObject.resultString.result)
                    dispatch(DisplayWord(resultObject.resultString.result));
                  }}
                  video={true}
                  render={({ status, startRecording, stopRecording, mediaBlobUrl, previewStream }) => (
                    <div className="videoBox">
                      <p>Status: {status}</p>
                      <VideoPreview stream={previewStream} />
                      <div className="beautifulButton">
                      <button onClick={startRecording}>Start Recording</button>
                      <button onClick={stopRecording}>Stop Recording</button>
                      </div>
                      {/* <video src={mediaBlobUrl as any} width={500} height={500} controls /> */}
                    </div>
                  )}
                />
              </div>
            </div>

            <div className="Section-4">
              <FileUploader />
            </div>
          </div>

          <div className="Section-5">
            <div className="Section-6">
                <div className="cantoneseTrans">你係咪講緊⋯</div>
                  <div className="currentWord">{currentWord.currentWord}</div>
            </div>
          </div>
        </div>              
      </div>
    </>
)}



export default App;

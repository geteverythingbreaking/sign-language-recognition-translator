import {createStore, combineReducers} from "redux";
import {reducer, WordState} from "./redux/reducer"
import {DisplayWord} from "./redux/action"

export interface IRootState{
    word:WordState
}

export type RootActions = DisplayWord

const rootReducer = combineReducers<IRootState>(
    {
    word:reducer
    } 
) 


export const store =  createStore<IRootState,RootActions,{},{}>(rootReducer);
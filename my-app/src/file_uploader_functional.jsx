import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { DisplayWord } from './redux/action'

export function FileUploader() {
    const dispatch = useDispatch();
    const [selectedFile, setSelectedFile] = useState(null);
    // const [loaded, setLoaded] = useState(0);

    return (
        <div className="Upload-Box">
            <div class="container">
                <div class="row">
                    <div class="col-md-11">
                        <form method="post" action="#" id="#">
                            <div class="form-group files">
                                <label> Upload Your Video here </label>
                                <input type="file" class="form-control" multiple="" name="file" onChange={event => {
                                    setSelectedFile(event.target.files[0]);
                                    // setLoaded(0)
                                }}
                                />
                            </div>
                            <button type="button" class="btn btn-success btn-block" onClick={async () => {
                                const fileData = new FormData()
                                fileData.append('profile', selectedFile)
                                const res = await fetch('http://localhost:8080/newPost', {
                                    method: "POST",
                                    body: fileData,
                                });
                                // console.log(res)
                                const fileResult = await res.json()
                                console.log('result', fileResult.resultString.result)
                                dispatch(DisplayWord(fileResult.resultString.result))
                            }}>Upload</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

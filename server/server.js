const fetch = require('node-fetch');
const express = require('express')
const bodyParser = require('body-parser')
const multer = require('multer');
// const dotenv = require('dotenv')
const cors = require('cors');
const { get } = require('http');


const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors({
    origin: [
        'http://localhost:3000']
}))

//Define the file path
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
const upload = multer({ storage })

// save the file (WebCam and Video)
app.post('/newPost', upload.single('profile'), async (req, res) => {
    console.log('==========')
    console.log(req.file)
    console.log(req.file.path)


    // fetch to app.py
    const resTest = await fetch("http://127.0.0.1:5000/predict",{
        headers:{
            "Content-Type": "application/json"
       },
        method: "POST",
        body: JSON.stringify(req.file)
    });
    console.log(`resTest: ${resTest.status}`)
    const resultString = await resTest.json()
    console.log(resultString.result)
    res.json({success: true, resultString})
    
})

// server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`[info] listening to Port: ${PORT}`);
});

import flask
from flask import request
from predict import prediction
import os
import json

app = flask.Flask(__name__,template_folder='Users/rush/Desktop/RNN/my-app/public')


@app.route("/predict", methods=["POST"])
def predict():

    content = request.get_json()

    input_data_path = content['path']
    output_data_path = "/Users/rush/Desktop/RNN/feature/"

    result_string = prediction(input_data_path,output_data_path)
    result_dict = {"result":result_string}

    return json.dumps(result_dict)



if __name__=="__main__":
    # For local development, set to True:
    app.run(debug=False)
    app.run()

#FLASK_APP=app.py flask run

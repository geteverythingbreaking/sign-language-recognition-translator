from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
from tensorflow import keras
from keras.preprocessing import sequence
from keras.datasets import imdb
from keras import layers, models
from keras.models import Sequential
from keras import layers
import os
import sys
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
import random
from keras import optimizers
from keras.layers import SimpleRNN, Dense
from keras.layers import Bidirectional
from numpy import argmax
import argparse
import datetime

def load_data(dirname):
    '''Assume a string input of the file location of video under the root /Users/rush/Desktop/RNN/feature/Absolute/video/
       Return a tuple (X, Y) where X is the landmark frame and Y is the label, default as video
    '''
    X = []
    Y = []
    wordname='video'

    textname = "/Users/rush/Desktop/RNN/feature/Absolute/video/"+dirname
    # processing X frames
    with open(textname, mode = 'r') as t:
        numbers = [float(num) for num in t.read().split()]
        # Fixed to 300 Frame
        # for i in range(len(numbers),25200):
        #     numbers.extend([0.000]) 
    landmark_frame=[]
    row=0
    #range(70)
    for i in range(int(len(numbers)/84)):
        landmark_frame.extend(numbers[row:row+84])
        row += 84
    landmark_frame=np.array(landmark_frame)
    landmark_frame=landmark_frame.reshape(-1,84)
    X.append(np.array(landmark_frame))
    Y.append(wordname)

    X=np.array(X)
    Y=np.array(Y)
    return X,Y


#prediction
def load_label():
    '''This function take no input and load the label of trained model 
       return a dict with 
       label as key,  as value like {'一起': 1}
    '''
    listfile=[]
    with open("./label.txt",mode='r') as l:
        listfile=[i for i in l.read().split()]
    label = {}
    count = 1
    for l in listfile:
        label[count] = l
        count += 1
    return label

def ff(input_data_path,video_data_path):
    '''Assume two string input, file location of an octet-stream file and preferred output file location of ffmpeg,
       The function call the os to process the input octet-stream file into a 28 fps .mp4 file by ffmpeg 
       and save it at the output file location
       This function return none  
    '''
    ffcmd = 'ffmpeg -i '+input_data_path+' -filter:v fps=fps=28 -y /Users/rush/Desktop/RNN/upload/video/'+video_data_path 
    os.system(ffcmd)

def prediction(input_data_path,output_data_path):
    '''Assume two string input, file location of an octet-stream file and preferred output file location of mediapipe,
       return a string which is the prediction of the input handsign video
    '''
    #Compiler
    comp='bazel build -c opt --define MEDIAPIPE_DISABLE_GPU=1 \mediapipe/examples/desktop/multi_hand_tracking:multi_hand_tracking_cpu'
    #MediaPipe Command
    cmd='GLOG_logtostderr=1 bazel-bin/mediapipe/examples/desktop/multi_hand_tracking/multi_hand_tracking_cpu \--calculator_graph_config_file=mediapipe/graphs/hand_tracking/multi_hand_tracking_desktop_live.pbtxt'

    x = str(datetime.datetime.now().timestamp())
    video_path = f"video_{x}.mp4"
    text_path = f"video_{x}.txt"

    #ffmpeg
    ff(input_data_path,video_path)

    os.system(comp)

    inputfilen='   --input_video_path=/Users/rush/Desktop/RNN/upload/video/'+video_path
    outputfilen='   --output_video_path=/Users/rush/Desktop/RNN/feature/_video/'+video_path
    
    cmdret=cmd+inputfilen+outputfilen
    os.system(cmdret)
    
    # Input to the model:
    x_test,Y=load_data(text_path)
    # Load model:
    new_model = tf.keras.models.load_model('/Users/rush/Desktop/RNN/mediapipe/rnnModelEp500Batch10Frame70')
    # Load word:
    labels=load_label()

    xhat = x_test
    yhat = new_model.predict(xhat)

    # Prediction:
    predictions = np.array([np.argmax(pred) for pred in yhat])

    outputString = ""
    # s=0
    for i in predictions:
        outputString = outputString + labels[i]
        # s+=1

    print(outputString)   
    return outputString 

